require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    correct_base_title = "Ruby on Rails Tutorial Sample App"
    assert_equal full_title,         correct_base_title
    assert_equal full_title("Help"), "Help | #{correct_base_title}"
  end
end